# Adapted from https://nordisch.org/posts/php-fpm-apparmor/

abi <abi/3.0>,

  /var/log/nextcloud-server/error rw,
  # we need to be able to create the nextcloud socket
  /run/nextcloud-server/php-fpm.sock rwlk,

  # this is the actual configuration file
  /etc/nextcloud/nextcloud-fpm.conf r,

profile nextcloud-server flags=(attach_disconnected) {
  # load common php worker rules
  include <abstractions/php-worker>
  # read openssl configuration
  include <abstractions/openssl>
  # read the system certificates
  include <abstractions/ssl_certs>
  # resolve hostnames/usernames
  include <abstractions/nameservice>
  # nextcloud uses fonts, apparently
  include <abstractions/fonts>

  # I don't know why this is being called when it is, but
  # ImagMagick is used
  /etc/ImageMagick*/** r,
  /usr/share/ImageMagick*/** r,

  /var/log/nextcloud-server/error rw,
  /var/log/nextcloud-server/log rw,

  # allow it to read our php files
  /etc/nextcloud/** lrwk,
  /etc/nextcloud/ r,
  /run/nextcloud-server/** klrw,
  /run/nextcloud-server/ r,
  /var/lib/nextcloud-server/** klrw,
  /var/lib/nextcloud-server/ r,
  /usr/share/nextcloud-server/** r,
  /usr/share/nextcloud-server/ r,

  # used by inkscape
  /usr/share/inkscape/** r,
  /usr/share/inkscape/ r,

  # notify_push
  /var/lib/nextcloud-server/apps/notify_push/bin/x86_64/notify_push ix,

  # FIXME: We can definitely tighten these up
  /usr/bin/* rix,
  /bin/* rix,

  @{PROC}/@{pid}/cgroup r,

  include if exists <local/nextcloud-server>
}
