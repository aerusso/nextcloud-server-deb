Debian integration of Nextcloud Server.

# Official Resources
  * [Nextcloud Website](https://nextcloud.com)
  * [Nextcloud Git Repository](https://github.com/nextcloud)

# Highlights
  * Runs as unprivileged user, with an apparmor profile.
  * In-place snapshot, compatible with filesystem snapshotting.  **VERY EXPERIMENTAL**!
  * Well-integrated: you aren't installing a second set of dependencies.
  * We download Nextcloud Server from the official site, check its
    signature, and then install it.  Packaging and upstream are __decoupled__.
  * Small footprint: most content is kept in a squashfs.
  * Authentication to mysql is via unix socket.
  * Run `nextcloud-server upgrade` to perform an upgrade: it downloads the
    latest version, and runs all the steps.
  * Redis memcache
  * Client push (`notify_push`)

# Important!
This requires apparmor! See the [Debian AppArmor Howto](https://wiki.debian.org/AppArmor/HowToUse).
Be sure to reboot after changing the grub command line.

Client push configuration requires some additional work.  See the `install-notify_push` section of
`man nextcloud-server`.

# Using the repository

**Please note this has changed recently!**

Add the following apt-source.list line:

> deb https://repo.aerusso.net/apt bookworm main

For old releases, add `release`.  To test the next release candidate, beta, or alpha version,
use `rc`, `beta`, or `alpha`, respectively.  Each of these will also pull in more mature releases,
i.e., `beta` will also pull in release candidates if there is a more mature one available.

At this point, bookworm amd64 is supported.  Bullseye ships a version of PHP that is not supported
by newer versions of Nextcloud, so no more work will be done on the bullseye packaging, and users
should upgrade to bookworm.

# Upgrading from bullseye

Nextcloud versions earlier than 26 do not support the version of PHP in bookworm, so the upgrade
to bookworm requires using Nextcloud 26.0.0, or later.  Be sure to bump the `deb` line to bookworm!
After the upgrade to bookworm and version `0.1.15` or later of this package, run `nextcloud-server update`
to install a compatible version of Nextcloud.

Adding another architecture will require testers.
Volunteer by opening an [issue](https://gitlab.com/aerusso/nextcloud-server-deb/-/issues)!

Use my posted gpg key to authenticate, either here or [my site](https://www.antonioerusso.com).
Please don't trust random repositories!

# Installing from source

```bash
git clone https://gitlab.com/aerusso/nextcloud-server-deb.git
cd nextcloud-server-deb
dpkg-buildpackage -b -us -uc
cd ..
sudo apt install ./nextcloud-server_*.deb
```

# Compatibility Notes

 * I have an install that I am maintaining with this, so I am making
   sure upgrades are __possible__.
 * Upgrade failures are not bugs (yet). I'll post notes about
   how to fix things.

# License and Copying

See `debian/copyright` for details.  In short, the project is available under the AGPLv3
license, the same as Nextcloud itself.
