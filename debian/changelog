nextcloud-server (0.1.16) UNRELEASED; urgency=medium

  * Track upstream nginx changes and new dependencies
  * Provide download-checklates as a mechanism to check for updates
  * Avoid trying to change the webroot (and failing the installation)
  * Tune the opcache
  * Indicate failure vs new version in download-getlatest
  * Tighten restrictions on notify-push socket

 -- Antonio Russo <aerusso@aerusso.net>  Thu, 09 May 2024 05:22:05 -0600

nextcloud-server (0.1.15) UNRELEASED; urgency=medium

  * Update README
  * Fix some lintian complaints
  * Hints for Nextpush
  * Documentation fixes
  * Bump PHP dependency from 7.4 to 8.2 for bookworm
  * Migrate to upstreamed apparmor profiles and 3.0 ABI
  * Better handling of version checks
  * (Hopefully) more robust upgrade logic
  * Refresh redis.conf and tighten permissions
  * Reduce requirements on certbot and its nginx plugin
  * Consolidate changelog entries

 -- Antonio Russo <aerusso@aerusso.net>  Fri, 09 Jun 2023 07:55:49 -0600

nextcloud-server (0.1.14) UNRELEASED; urgency=medium

  * Refresh nginx configuration
  * Check for ssl flag in nginx.conf

 -- Antonio Russo <aerusso@aerusso.net>  Sat, 10 Dec 2022 05:56:42 -0700

nextcloud-server (0.1.13) UNRELEASED; urgency=medium

  * The repository has changed! See the README
  * Redis memcache
  * notify_push support
    - ALL USERS: read man nextcloud-server (install-notify_push)
  * All logs now go under /var/log/nextcloud-server
  * Split up nginx configuration
  * Increase default limits on php
  * Apply lintian-brush recommendations (thanks James Valleroy)
  * More lintian cleanups
    - Document nextcloud-server-snapshot.service
    - Properly use dpkg-maintscript
    - Ignore inappropriate lintian warnings
  * Refresh my gpg key
  * Reformat documentation
  * Rework systemd dependencies
  * php-imagick svg support
  * better safety around unversioned restore
  * more precise purge behavior
  * snapshots: stop notify_push
  * do not forbid logging while under php-fpm//nextcloud apparmor hat
  * snapshot: avoid race with nextcloud-periodic
  * snapshot: fix rsync failures
  * cleanly enable apc on the command line
  * Cleanup/typo fixes

 -- Antonio Russo <aerusso@aerusso.net>  Wed, 21 Jul 2021 22:29:28 -0600

nextcloud-server (0.1.12) UNRELEASED; urgency=medium

  * Depend on bzip2/lbzip2
  * Loudly fail for bad signature
  * Specify php version once
  * Move to dh_compat 14
  * No ssl over port 80
  * man page typo
  * Make arch:any
  * Stop tracking php-versioned apparmor profile
  * Add new php dependencies
  * backport upstream apparmor profile
  * Fix backup snapshot rsync failure
  * add nextcloud-server-snapshot.service
  * Add example backup hook
  * Update my email
  * Update copyright information

 -- Antonio Russo <aerusso@aerusso.net>  Sat, 04 Jul 2020 15:13:26 -0600

nextcloud-server (0.1.10) UNRELEASED; urgency=medium

  * Drop privileges with setpriv, rather than su.

 -- Antonio Russo <antonio.e.russo@gmail.com>  Sun, 26 Apr 2020 15:54:35 -0600

nextcloud-server (0.1.9) UNRELEASED; urgency=medium

  * Improve documentation.
  * Handle periodic timer.
  * /etc/nextcloud may not exist at pkg configure time.

 -- Antonio Russo <antonio.e.russo@gmail.com>  Sat, 07 Mar 2020 10:05:01 -0700

nextcloud-server (0.1.8) UNRELEASED; urgency=medium

  * php-fpm underflow, CVE-2019-11043.

 -- Antonio Russo <antonio.e.russo@gmail.com>  Sat, 26 Oct 2019 07:01:50 -0600

nextcloud-server (0.1.7) UNRELEASED; urgency=medium

  * Fix several typos.

 -- Antonio Russo <antonio.e.russo@gmail.com>  Thu, 03 Oct 2019 18:32:10 -0600

nextcloud-server (0.1.6) UNRELEASED; urgency=medium

  * Fix 2-factor login issue.
  * Lock down extraneous systemd units starting in slice.
  * Inkscape apparmor rules.
  * Correctly preserve downloaded tar files, upon request.
  * More consistently wait for periodic service to complete.

 -- Antonio Russo <antonio.e.russo@gmail.com>  Wed, 04 Sep 2019 18:00:55 -0600

nextcloud-server (0.1.5) UNRELEASED; urgency=medium

  * Fix typo in service file.
  * Use cron instead of systemd-timers (should quiet some logging noise).

 -- Antonio Russo <antonio.e.russo@gmail.com>  Tue, 03 Sep 2019 07:14:42 -0600

nextcloud-server (0.1.4) UNRELEASED; urgency=medium

  * Rework unit startup, hopefully simplifying things a lot
  * Should works on startup now
  * Preserve custom files

 -- Antonio Russo <antonio.e.russo@gmail.com>  Mon, 02 Sep 2019 20:01:24 -0600

nextcloud-server (0.1.3) UNRELEASED; urgency=medium

  * Basic logging.
  * Improve manual management.
  * Better php environment sanitization.
  * Minor simplifications.
  * Should clear up guzzlehttp CurlHandler temp directory issue.
  * Address several restore issues.

 -- Antonio Russo <antonio.e.russo@gmail.com>  Sun, 01 Sep 2019 08:48:27 -0600

nextcloud-server (0.1.2) UNRELEASED; urgency=medium

  * Properly configure package.
  * Correct nextcloud-server exec environment.
  * Packaging cleanup.
  * Minor documentation improvements.
  * Simplify maintainer scripts.

 -- Antonio Russo <antonio.e.russo@gmail.com>  Sat, 31 Aug 2019 18:39:20 -0600

nextcloud-server (0.1.1) UNRELEASED; urgency=medium

  * Backup and restore fixes
  * Better webserver configuration checks
  * Quiet some garbage output

 -- Antonio Russo <antonio.e.russo@gmail.com>  Tue, 27 Aug 2019 17:57:59 -0600

nextcloud-server (0.1) UNRELEASED; urgency=medium

  * Move to native format
  * Manage downloads at runtime, instead of compile-time. Specifically:
    - nextcloud-server upgrade

 -- Antonio Russo <antonio.e.russo@gmail.com>  Sun, 25 Aug 2019 21:28:05 -0600

nextcloud-server (0~16.0.4~beta2-1) UNRELEASED; urgency=medium

  * Add upgrade logic.
  * Allow for in-place restores, and specifically at install-time.
  * Add man page.
  * Apparmor improvements:
    - refactor to separate php-fpm from nextcloud
    - add local drop-in for e.g. external storages
    - Fail early if apparmor isn't enabled

 -- Antonio Russo <antonio.e.russo@gmail.com>  Sun, 25 Aug 2019 13:11:19 -0600

nextcloud-server (0~16.0.4~beta1-1) unstable; urgency=medium

  * Initial release

 -- Antonio Russo <antonio.e.russo@gmail.com>  Sat, 24 Aug 2019 08:18:57 -0600
