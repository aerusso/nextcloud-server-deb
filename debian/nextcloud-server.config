#!/bin/sh

set -e
mkdir -p /etc/nextcloud
chmod 1770 /etc/nextcloud

do_fail() {
    printf "%s\n" "$1"
    exit 1
}

# Source debconf library
. /usr/share/debconf/confmodule

if dpkg -S /etc/apparmor.d/abstractions/php-worker|grep -q nextcloud-server ; then
    # This is a horrible hack
    [ -f /etc/apparmor.d/abstractions/php-worker ] && mv /etc/apparmor.d/abstractions/php-worker /etc/apparmor.d/abstractions/php-worker.dpkg-remove
fi

if grep -q '^[^#]\+https://www.antonioerusso.com/repositories/debian' -r /etc/apt/sources.list* 2>/dev/null ;  then
    db_input critical nextcloud-server/warn-repo-change || true
    db_go
fi

db_input high nextcloud-server/auto || true
db_go

db_get nextcloud-server/auto || true
# if we aren't performing automatic handling, we can stop.
if [ "$RET" = true ] ; then

db_input high nextcloud-server/restore || true
db_go
db_get nextcloud-server/restore || true

if [ "$RET" = true ] ; then
    # We're doing a restore.
    db_set nextcloud-server/install false
    db_fset nextcloud-server/install seen true
    db_fset nextcloud-server/version seen true
elif [ -e /etc/nextcloud/VERSION ] ; then
    # We're installing over a previously configured system.
    db_set nextcloud-server/version ""
    db_fset nextcloud-server/version seen true
else
    # We're doing a fresh install

    db_input high nextcloud-server/version || true
    db_input high nextcloud-server/install || true
    db_go

    # Nothing more to do if we're not installing
    db_get nextcloud-server/install || true
    [ "$RET" = true ] || exit 0

    db_input high nextcloud-server/domain || true
    db_input high nextcloud-server/admin-password || true
    db_go

    mkdir -p /etc/nextcloud
    chmod 1770 /etc/nextcloud

    # We don't keep these in debconf
    db_get nextcloud-server/domain || true
    if [ -n "${RET}" ] ; then
        printf '%s' "${RET}" >/etc/nextcloud/domain
        db_reset nextcloud-server/domain
    else
        [ -f /etc/nextcloud/domain ] || do_fail "Domain must not be empty"
    fi

    db_get nextcloud-server/admin-password || true
    if [ -n "${RET}" ] ; then
        printf '%s' "${RET}" >/etc/nextcloud/admin-password
        db_reset nextcloud-server/admin-password
    else
         [ -f /etc/nextcloud/admin-password ] || do_fail "Password must not be empty"
    fi

fi ; fi

#DEBHELPER#
