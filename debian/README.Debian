nextcloud-server
----------------

As of now, both 26.0.0 and 26.0.1 exist and work with the version of PHP (8.2)
present in bookworm.  Simply run

    nextcloud-server upgrade

after finishing the bullseye -> bookworm upgrade.

 -- Antonio Enrico Russo <aerusso@aerusso.net>  Sun, 23 Apr 2023 18:18:26 -0600

nextcloud-server
----------------

This version adds an early-fail test of PHP compatibility, which before now
was never an issue.  At the time of writing, no versions of Nextcloud support
php 8.2.  Nextcloud 26 *should* support 8.0 through 8.2, meaning that you
will have to upgrade Nextcloud after upgrading to bookworm.  The other option
is to use the version check override:

    systemctl enable nextcloud-server-disable-versioncheck.service

I do NOT recommend this.  Instead, I am personally using a release candidate
of Nextcloud 26 (rc3).  This can be installed by calling

    WEBURL='https://download.nextcloud.com/server/prereleases/' nextcloud-server upgrade 26.0.0rc3

Though, hopefully by the time you're reading this, 26 has already been
released and you can just do a regular nextcloud-server upgrade.
You can check that URL to get the available versions.

 -- Antonio Enrico Russo <aerusso@aerusso.net>  Sun, 19 Mar 2023 08:43:07 -0600

nextcloud-server
----------------

Initial release! I've attempted to make everything behave as smoothly as
possible. Some things to note, that are different than default nextcloud
installations:

 1. Configuration information is in /etc/nextcloud .
 2. Most of the web files are kept as a squashfs, mounted at
    /usr/share/nextcloud-server/webroot .
 3. Parts of the website that change are in /var/lib/nextcloud-server .
 4. Authentication to the sql server is via unix_socket, not password!
 5. The sql database is symlinked from /var/lib/nextcloud-server/sql.
 6. Almost everything is done under the nextcloud system user.
 7. The php-fpm server is SET TO RUN WITH AN APPARMOR PROFILE
    /etc/apparmor.d/usr.sbin.php-fpm*
    and changes its 'hat' to
    /etc/apparmor.d/php-fpm.d/nextcloud-server
    You can set either one of these to "complain" to disable.

Hopefully the auto-install worked for you.  You can also run the install
manually by calling

    nextcloud-server install

The web-upgrade is disabled--upgrade by upgrading the deb file instead.
To access occ, use

    nextcloud-server occ <whatever>

To STOP all the nextcloud services, run

    nextcloud-server stop

If you want to refresh the web-configuration, because, e.g., you changed
/etc/nginx/sites-enabled/nextcloud, and you want it reverted, call

    nextcloud-server refresh-webconf

Also, I haven't set up any redis server yet.  There is an EXPERIMENTAL
snapshot backup process.  See

    nextcloud-server help snapshot-lock

and

    nextcloud-server help restore-prepare

If you're reinstalling nextcloud-server to restore a backup snapshot, make
sure you're running the EXACT SAME mysql version.  Decline auto-configuration
and use nextcloud-server restore-* . Because almost everything is kept in
place, snapshots should be cheap. Try to integrate them into your, e.g. zfs,
snapshotting! Report bugs please.

 -- Antonio Enrico Russo <aerusso@aerusso.net>  Sun, 18 August 2019 20:09:12 -0600
