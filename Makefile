#! /bin/make -f

all:
	[ -n $(PHP_VER) ]
	find tree -type f -name '*.in' -exec scripts/process $(PHP_VER) {} \;
	mkdir -p tree/usr/share/man/man8
	printf '%% NEXTCLOUD-SERVER(8) %s | Administrative Documentation\n\n' "$(shell dpkg-parsechangelog -l debian/changelog | sed -n 's/^Version: //p')" >manfile-tmp.md
	cat nextcloud-server.8.md >>manfile-tmp.md
	pandoc --standalone --to man manfile-tmp.md -o tree/usr/share/man/man8/nextcloud-server.8


install:
	find tree -type f ! -name '*.in' -exec scripts/install {} $(DESTDIR)$(PREFIX)  \;

clean:
	rm -f manfile.tmp
	find tree -type f -name '*.in' -exec scripts/clean {} \;
	rm -f tree/usr/share/man/man8/nextcloud-server.8
	[ ! -e tree/usr/share/man/man8 ] || rmdir tree/usr/share/man/man8
	[ ! -e tree/usr/share/man ] || rmdir tree/usr/share/man
