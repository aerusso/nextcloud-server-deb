# This file is included by /etc/nginx/sites-available/nextcloud
# It contains settings from upstream's (unofficial and unsupported) nginx configuration:
# https://docs.nextcloud.com/server/latest/admin_manual/installation/nginx.html
# as well as some changes I have made.  I will track upstreams changes during updates.

# I have isolated these values so that you may change them.

# By remove `#enable ssl` below, the nextcloud-server command will NOT automatically run certbot:
# NOTE: You may need to modify /etc/nginx/sites-available/nextcloud and/or call
# nextcloud-server refresh-webconf-force to regenerate that file.
#enable ssl

# ssl stapling
ssl_stapling on;
ssl_stapling_verify on;
resolver 8.8.8.8 8.8.4.4 valid=10m;
resolver_timeout 10s;

# HSTS settings
# WARNING: Only add the preload option once you read about
# the consequences in https://hstspreload.org/. This option
# will add the domain to a hardcoded list that is shipped
# in all major browsers and getting removed from this list
# could take several months.
#add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload" always;

# set max upload size and increase upload timeout
client_max_body_size 768M;
client_body_timeout 600s;
fastcgi_buffers 64 4K;

# logging
access_log /var/log/nextcloud-server/nginx;

# Enable gzip but do not remove ETag headers
gzip on;
gzip_vary on;
gzip_comp_level 4;
gzip_min_length 256;
gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;
gzip_types application/atom+xml text/javascript application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/wasm application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;

# Pagespeed is not supported by Nextcloud, so if your server is built
# with the `ngx_pagespeed` module, uncomment this line to disable it.
#pagespeed off;

# The settings allows you to optimize the HTTP2 bandwidth.
# See https://blog.cloudflare.com/delivering-http-2-upload-speed-improvements/
# for tuning hints
client_body_buffer_size 512k;

# Prevent nginx HTTP Server version advertising
server_tokens off;

# Uncomment this line to expose a Matrix the push gateway implemented by Nextpush
#location /_matrix/push/v1/notify {proxy_pass https://$server_name/index.php/apps/uppush/gateway/matrix;}
