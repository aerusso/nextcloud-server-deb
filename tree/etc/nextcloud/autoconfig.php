<?php

$data_path = '/var/lib/nextcloud-server/data';

$database_password = trim(file_get_contents('/etc/nextcloud/dbpasswd'));

$AUTOCONFIG = array(
'directory' => getenv('/var/lib/nextcloud-server/data'),

'dbtype' => 'mysql',

'dbhost' => 'localhost:/run/mysqld/mysqld.sock',

'dbname' => 'nextcloud',

'dbuser' => 'nextcloud',

'dbpass' => $database_password,
);
